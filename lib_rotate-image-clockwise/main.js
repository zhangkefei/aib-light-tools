function rotate(file, degree) {
    return new Promise(async resolve => {
        const imageBitmap = await createImageBitmap(file)
        const { width, height } = imageBitmap
        let canvas
        if ([90, 270].includes(degree)) {
            canvas = new OffscreenCanvas(height, width)
            const ctx = canvas.getContext('2d')
            ctx.translate(height / 2, width / 2)
            ctx.rotate((degree * Math.PI) / 180)
            ctx.drawImage(imageBitmap, -width / 2, -height / 2)
        } else if ([180, 360].includes(degree)) {
            canvas = new OffscreenCanvas(width, height)
            const ctx = canvas.getContext('2d')
            ctx.translate(width / 2, height / 2)
            ctx.rotate((degree * Math.PI) / 180)
            ctx.drawImage(imageBitmap, -width / 2, -height / 2)
        }
        const blob = await canvas.convertToBlob({
            type: 'image/png',
        })
        const resultFile = new File([blob], 'rotated.png', {
            type: 'image/png',
        })
        resolve(resultFile)
    })
}

export default async (file, degree) => {
    try {
        if (!file.type.startsWith('image/')) {
            $.error('这不是图片文件')
            return
        }
        degree = Number(degree)
        if (isNaN(degree) || ![90, 180, 270, 360].includes(degree)) {
            $.error('旋转角度应该是90、180、270、360其中之一')
            return
        }
        return await rotate(file, degree)
    } catch (error) {
        $.error(error.message)
    }
}
