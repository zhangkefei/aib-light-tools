import qrcode from "qrcode";

function toQrcodeImageFile(str) {
    return new Promise(async resolve => {
        let canvas
        if (typeof document !== 'undefined') {
            // in main thread
            canvas = document.createElement('canvas')
            const r = await qrcode.toCanvas(canvas, str)
            r.toBlob(function (blob) {
                resolve(new File([blob], 'result.jpeg', { type: 'image/jpeg' }))
            }, 'image/jpeg', 0.95)
        } else {
            // in web worker
            canvas = new OffscreenCanvas(256, 256);
            const r = await qrcode.toCanvas(canvas, str)
            const blob = await r.convertToBlob({
                type: 'image/jpeg',
                quality: 0.95
            })
            resolve(new File([blob], 'result.jpeg', { type: 'image/jpeg' }))
        }
    })
}

function dataURLToBlob(dataurl) {
    const type = dataurl.match(/data:(.+);/)[1];
    const base64 = dataurl.split(',')[1];
    const binStr = atob(base64);
    const u8a = new Uint8Array(binStr.length);
    let p = binStr.length;
    while (p) {
        p--;
        u8a[p] = binStr.codePointAt(p);
    }
    return new Blob([u8a], { type });
}

export default async (str) => {
    try {
        // console.log('window:', window);
        // console.log('self:', self);
        // console.log(typeof document);
        // return str
        return toQrcodeImageFile(str)
        // const dataUrl = await qrcode.toDataURL(str)
        // const blob = dataURLToBlob(dataUrl)
        // const file = new File([blob], 'result.png', { type: 'image/png' })
        // return file
    } catch (error) {
        console.error(error);
        $.message('error')
    }
}