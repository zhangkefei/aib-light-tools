
import convert from "color-convert";

function isHex(color) {
    return /^#(\d|[abcdef]){6}$/.test(color)
}

function isRgb(color) {
    return /^(\d|\d\d|[1]\d\d|[2][0-5][0-5]),(\d|\d\d|[1]\d\d|[2][0-5][0-5]),(\d|\d\d|[1]\d\d|[2][0-5][0-5])$/.test(color)
}

function isHsl(color) {
    return /^(\d|\d\d|[123][0-5][0-9]),(\d|\d\d|[1][0][0])%,(\d|\d\d|[1][0][0])%$/.test(color)
}

export default (colorStr) => {
    try {
        if (isHex(colorStr)) {
            $.result(`rgb: ${convert.hex.rgb(colorStr).join(',')}`)
            $.result(`hsl: ${convert.hex.hsl(colorStr).join(',')}`)
        } else if (isRgb(colorStr)) {
            const color = colorStr.split(',')
            $.result(`hex: #${convert.rgb.hex(color)}`)
            $.result(`hsl: ${convert.rgb.hsl(color).join(',')}`)
        } else if (isHsl(colorStr)) {
            const color = colorStr.replace(/%/g, '').split(',')
            $.result(`hex: #${convert.hsl.hex(color)}`)
            $.result(`rgb: ${convert.hsl.rgb(color).join(',')}`)
        } else {
            $.result(`hex: #${convert.keyword.hex(colorStr)}`)
            $.result(`rgb: ${convert.keyword.rgb(colorStr).join(',')}`)
            $.result(`hsl: ${convert.keyword.hsl(colorStr).join(',')}`)
        }
    } catch (error) {
        console.error(error);
        $.message('无法识别的颜色值')
    }
}