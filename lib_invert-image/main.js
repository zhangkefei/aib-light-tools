function invert(file) {
    return new Promise(async resolve => {
        const imageBitmap = await createImageBitmap(file)
        const { width, height } = imageBitmap
        const canvas = new OffscreenCanvas(width, height)
        const ctx = canvas.getContext('2d');
        ctx.drawImage(imageBitmap, 0, 0, width, height);
        const imageData = ctx.getImageData(0, 0, width, height);
        const data = imageData.data;
        for (let i = 0; i < data.length; i += 4) {
            data[i] = 255 - data[i];
            data[i + 1] = 255 - data[i + 1];
            data[i + 2] = 255 - data[i + 2];
        }
        ctx.putImageData(imageData, 0, 0);
        const blob = await canvas.convertToBlob({
            type: 'image/png'
        });
        const resultFile = new File([blob], 'inverted.png', {
            type: 'image/png'
        });
        resolve(resultFile)
    })
}

export default async (file) => {
    try {
        if (!file.type.startsWith('image/')) {
            $.error('这不是图片文件')
            return
        }
        return await invert(file)
    } catch (error) {
        $.error(error.message)
    }
}