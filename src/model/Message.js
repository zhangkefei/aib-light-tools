import { v4 } from "uuid"

export class Message {
    constructor(msg) {
        this.message = msg
        this.id = v4()
    }
}