import { ResultMessage } from "./ResultMessage";

export class ResultFileMessage extends ResultMessage {
    constructor(file) {
        super(file.name)
        this._file = file
    }

    get file() {
        return this._file
    }

    get size() {
        return this._file.size
    }
}