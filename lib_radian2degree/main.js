export default (str) => {
    try {
        const radian = Number(str)
        if (isNaN(radian)) {
            $.error('请输入一个数字')
        } else {
            return (radian * (180 / Math.PI)) + ''
        }
    } catch (error) {
        $.error(error.message)
    }
}