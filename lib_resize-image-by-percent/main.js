function resize(file, ratio) {
    return new Promise(async resolve => {
        const imageBitmap = await createImageBitmap(file)
        const { width, height } = imageBitmap
        ratio = ratio / 100
        const canvas = new OffscreenCanvas(width * ratio, height * ratio)
        const ctx = canvas.getContext('2d');
        ctx.scale(ratio, ratio)
        ctx.drawImage(imageBitmap, 0, 0, width, height);
        const blob = await canvas.convertToBlob({
            type: 'image/jpeg',
            quality: 0.75
        });
        const resultFile = new File([blob], 'resized.jpg', {
            type: 'image/jpeg'
        });
        resolve(resultFile)
    })
}

export default async (file, ratio) => {
    try {
        if (!file.type.startsWith('image/')) {
            $.error('这不是图片文件')
            return
        }
        ratio = Number(ratio)
        if (isNaN(ratio) || ratio <= 0) {
            $.error('比例值应该是一个大于0的数字')
            return
        }
        return await resize(file, ratio)
    } catch (error) {
        $.error(error.message)
    }
}