import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [vue({
    template: {
      compilerOptions: {
        isCustomElement: tag => tag.startsWith('sl-')
      }
    }
  })],
  build: {
    lib: {
      entry: 'lib/main.js',
      name: 'main',
      formats: ['iife'],
      fileName: 'main'
    },
    copyPublicDir: false,
    write: true
  }
})
