function resize(file, px) {
    return new Promise(async resolve => {
        const imageBitmap = await createImageBitmap(file)
        const { width, height } = imageBitmap
        const ratio = px / width
        const canvas = new OffscreenCanvas(width * ratio, height * ratio)
        const ctx = canvas.getContext('2d');
        ctx.scale(ratio, ratio)
        ctx.drawImage(imageBitmap, 0, 0, width, height);
        const blob = await canvas.convertToBlob({
            type: 'image/jpeg',
            quality: 0.75
        });
        const resultFile = new File([blob], 'resized.jpg', {
            type: 'image/jpeg'
        });
        resolve(resultFile)
    })
}

export default async (file, px) => {
    try {
        if (!file.type.startsWith('image/')) {
            $.error('这不是图片文件')
            return
        }
        px = Number(px)
        if (isNaN(px) || px <= 0) {
            $.error('宽度值应该是一个大于0的数字')
            return
        }
        return await resize(file, px)
    } catch (error) {
        $.error(error.message)
    }
}