import { resolve } from 'path'
import { writeFile, writeFileSync, readFileSync, renameSync } from 'fs'
const projectDir = resolve()
const pkgFile = resolve(projectDir, 'lib', 'package.json')
let pkg = readFileSync(pkgFile)
pkg = JSON.parse(pkg)
const { name, version, description, author, assistantInBrowser } = pkg
const buildPkg = { name, version, description, author, assistantInBrowser }

const distPkgFile = resolve(projectDir, 'dist', 'package.json')

writeFile(distPkgFile, JSON.stringify(buildPkg, null, '    '), err => {
    if (err) {
        console.error(err)
        return
    }
})

const sourceReadmeFile = resolve(projectDir, 'lib', 'README.md')
const distReadmeFile = resolve(projectDir, 'dist', 'README.md')
writeFileSync(distReadmeFile, readFileSync(sourceReadmeFile));

const oldMainFile = resolve(projectDir, 'dist', 'main.iife.js')
const newMainFile = resolve(projectDir, 'dist', 'main.js')
renameSync(oldMainFile, newMainFile)