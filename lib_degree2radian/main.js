export default (str) => {
    try {
        const degree = Number(str)
        if (isNaN(degree)) {
            $.error('请输入一个数字')
        } else {
            return (degree * (Math.PI / 180)) + ''
        }
    } catch (error) {
        $.error(error.message)
    }
}