import md5 from "md5";

export default (file) => {
    try {
        return new Promise((resolve, reject) => {
            const reader = new FileReader()
            reader.addEventListener('load', () => {
                const md5str = md5(reader.result)
                resolve(md5str)
            })
            reader.addEventListener('error', () => {
                reject('Error occurred reading file')
            })
            reader.readAsArrayBuffer(file)
        })
    } catch (error) {
        console.error(error);
        $.message('error')
    }
}