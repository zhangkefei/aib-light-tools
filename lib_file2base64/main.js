export default (file) => {
    try {
        return new Promise((resolve, reject) => {
            const reader = new FileReader()
            reader.addEventListener('load', () => {
                resolve(reader.result)
            })
            reader.addEventListener('error', () => {
                $.error('Error occurred reading file')
                reject('Error occurred reading file')
            })
            reader.readAsDataURL(file)
        })
    } catch (error) {
        console.error(error);
        $.error(error.message)
    }
}