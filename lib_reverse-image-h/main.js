function reverseH(file) {
    return new Promise(async resolve => {
        const imageBitmap = await createImageBitmap(file)
        const { width, height } = imageBitmap
        const canvas = new OffscreenCanvas(width, height)
        const ctx = canvas.getContext('2d');
        ctx.drawImage(imageBitmap, 0, 0, width, height);
        const imageData = ctx.getImageData(0, 0, width, height);
        const data = imageData.data;
        for (let hIndex = 0; hIndex < height; hIndex++) {
            const rowStart = hIndex * width * 4
            for (let i = 0; i < width / 2; i ++) {
                const oIndex = rowStart + i * 4
                const tIndex = rowStart + (width - 1 - i) * 4
                const o = data.slice(oIndex, oIndex + 4)
                const t = data.slice(tIndex, tIndex + 4)
                data.set(o, tIndex)
                data.set(t, oIndex)
            }
        }
        ctx.putImageData(imageData, 0, 0);
        const blob = await canvas.convertToBlob({
            type: 'image/png'
        });
        const resultFile = new File([blob], 'reversed.png', {
            type: 'image/png'
        });
        resolve(resultFile)
    })
}

export default async (file) => {
    try {
        if (!file.type.startsWith('image/')) {
            $.error('这不是图片文件')
            return
        }
        return await reverseH(file)
    } catch (error) {
        $.error(error.message)
    }
}