function zip(file) {
    return new Promise(async resolve => {
        const imageBitmap = await createImageBitmap(file)
        const { width, height } = imageBitmap
        const canvas = new OffscreenCanvas(width, height)
        const ctx = canvas.getContext('2d');
        ctx.drawImage(imageBitmap, 0, 0, width, height);
        const compressedData = await canvas.convertToBlob({
            type: 'image/jpeg',
            quality: 0.75
        });
        const compressedFile = new File([compressedData], 'compressed.jpg', {
            type: 'image/jpeg'
        });
        resolve(compressedFile)
    })
}

export default async (file) => {
    try {
        if (file.type.startsWith('image/')) {
            return await zip(file)
        } else {
            $.error('这不是图片文件')
        }
    } catch (error) {
        $.error(error.message)
    }
}