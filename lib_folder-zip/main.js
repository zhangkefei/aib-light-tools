import JSZip from "jszip";

async function addFile(zip, fileHandle) {
    const file = await fileHandle.getFile()
    zip.file(file.name, file)
}

async function addDirectory(zip, dirHandle) {
    for await (const value of dirHandle.values()) {
        if (value instanceof FileSystemDirectoryHandle) {
            const folder = zip.folder(value.name)
            await addDirectory(folder, value)
        } else if (value instanceof FileSystemFileHandle) {
            await addFile(zip, value)
        }
    }
}

export default async (dirHandle) => {
    try {
        const zip = new JSZip()
        await addDirectory(zip, dirHandle)
        const blob = await zip.generateAsync({type: "blob"})
        return new File([blob], dirHandle.name + '.zip')
    } catch (error) {
        console.error(error);
        $.message('error')
    }
}