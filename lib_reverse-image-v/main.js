function reverseH(file) {
    return new Promise(async resolve => {
        const imageBitmap = await createImageBitmap(file)
        const { width, height } = imageBitmap
        const canvas = new OffscreenCanvas(width, height)
        const ctx = canvas.getContext('2d');
        ctx.drawImage(imageBitmap, 0, 0, width, height);
        const imageData = ctx.getImageData(0, 0, width, height);
        const data = imageData.data;
        const getOneRowSlice = (imageData, rowIndex) => {
            const {data, width, height} = imageData
            if (rowIndex >= height) {
                return null
            } else {
                const rowStart = rowIndex * width * 4
                const rowEnd = rowStart + width * 4
                return data.slice(rowStart, rowEnd)
            }
        }
        for (let hIndex = 0; hIndex < height / 2; hIndex++) {
            const oIndex = hIndex
            const tIndex = height - 1 - oIndex
            const o = getOneRowSlice(imageData, oIndex)
            const t = getOneRowSlice(imageData, tIndex)
            data.set(t, oIndex * width * 4)
            data.set(o, tIndex * width * 4)
        }
        ctx.putImageData(imageData, 0, 0);
        const blob = await canvas.convertToBlob({
            type: 'image/png'
        });
        const resultFile = new File([blob], 'reversed.png', {
            type: 'image/png'
        });
        resolve(resultFile)
    })
}

export default async (file) => {
    try {
        if (!file.type.startsWith('image/')) {
            $.error('这不是图片文件')
            return
        }
        return await reverseH(file)
    } catch (error) {
        $.error(error.message)
    }
}