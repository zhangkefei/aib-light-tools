import JSZip from "jszip";
export default async (file) => {
    try {
        const zip = new JSZip()
        zip.file(file.name, file)
        const blob = await zip.generateAsync({type: "blob"})
        return new File([blob], file.name + '.zip')
    } catch (error) {
        console.error(error);
        $.message('error')
    }
}